var APP_DATA = {
    scenes: [
        {
            id: "0-first-room",
            name: "First Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 2.447607647925624,
                pitch: 0.0851035105350526,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.22226443917490712,
                    pitch: 0.2846670986562536,
                    rotation: 5.497787143782138,
                    target: "1-second-room",
                },
                {
                    yaw: 0.29975319159350633,
                    pitch: 0.2909177910294609,
                    rotation: 0.7853981633974483,
                    target: "10-eleventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 1.5459492601860996,
                    pitch: -0.23929514799464613,
                    title: "Raju",
                    text: "Hello! I am Raju, a small puppy. I love running around and playing with my friends. I ask the hoomans for teats once in a while and run away when I get them. People call my art one of a kind, so hurry up before my NFT gets away.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL1.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.332,
                    pitch: 0.013,
                    id: "#box",
                },

                {
                    yaw: 2.8158,
                    pitch: 0.017,
                    id: "#box",
                },
                {
                    yaw: 3.5124115563955277,
                    pitch: 0.01635741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.994,
                    pitch: 0.01378695413934899,
                    id: "#box",
                },
                {
                    yaw: 5.1176029481141825,
                    pitch: 0.01011,
                    id: "#box",
                },
                {
                    yaw: 4.4537,
                    pitch: 0.01419,
                    id: "#box",
                },
                {
                    yaw: 1.869827474434887,
                    pitch: 0.01,
                    id: "#box",
                },
                {
                    yaw: 1.213,
                    pitch: 0.01,
                    id: "#box",
                },
            ],
        },
        {
            id: "1-second-room",
            name: "Second Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0207909818550043,
                pitch: 0.00929991458684043,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7599954763863845,
                    pitch: 0.21248369994290073,
                    rotation: 13.351768777756625,
                    target: "0-first-room",
                },
                {
                    yaw: -0.7868785139556902,
                    pitch: 0.21220416868843905,
                    rotation: 5.497787143782138,
                    target: "2-third-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4977283834599042,
                    pitch: -0.2772742100912069,
                    title: "Cooper",
                    text: "Meet Cooper! Being an older and wise dog, Cooper loves lazing around and watching the other dogs play. Cooper spends most of his day daydreaming and watching the younger dogs.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL2.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "2-third-room",
            name: "Third Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0586062474880826,
                pitch: 0.06749046743565579,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7755301378086461,
                    pitch: 0.2416458274263018,
                    rotation: 14.137166941154074,
                    target: "1-second-room",
                },
                {
                    yaw: -0.7613071911478908,
                    pitch: 0.21913681386708284,
                    rotation: 4.71238898038469,
                    target: "3-fourth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4985241710767507,
                    pitch: -0.2724882825875756,
                    title: "Maple",
                    text: "Hello, I am Maple!  was rescued when I was a little pup, I was injured and limping down a dark street. A human came by and I shivered. Later I loved the cozy home I was brought into, I played with my friends all day and got treats when I painted. My NFT is much loved and will not wait for long, so get yours soon!",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL3.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "3-fourth-room",
            name: "Fourth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.3315696176881477,
                pitch: 0.24893189576589947,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.28411284054260477,
                    pitch: 0.2713907397401112,
                    rotation: 36.91371367968008,
                    target: "2-third-room",
                },
                {
                    yaw: 0.36513653945095115,
                    pitch: 0.2855236568097457,
                    rotation: 0.7853981633974483,
                    target: "4-fifth-room-",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.129196363607825,
                    pitch: -0.24465680493715247,
                    title: "Mowgli",
                    text: "Meet Mowgli! As his name sounds, he is a rescue dog from the woods. He had a rough start in the shelter. We initially rescued him with his little sister, later separated from her while a family adopted him to be homeless again. Later, he is now reunited with his sister and spends his day playing around and making his NFTs.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL4.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.332,
                    pitch: 0.013,
                    id: "#box",
                },

                {
                    yaw: 2.8158,
                    pitch: 0.017,
                    id: "#box",
                },
                {
                    yaw: 3.5124115563955277,
                    pitch: 0.01635741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.994,
                    pitch: 0.01378695413934899,
                    id: "#box",
                },
                {
                    yaw: 5.1176029481141825,
                    pitch: 0.01011,
                    id: "#box",
                },
                {
                    yaw: 4.4537,
                    pitch: 0.01419,
                    id: "#box",
                },
                {
                    yaw: 1.869827474434887,
                    pitch: 0.01,
                    id: "#box",
                },
                {
                    yaw: 1.213,
                    pitch: 0.01,
                    id: "#box",
                },
            ],
        },
        {
            id: "4-fifth-room-",
            name: "Fifth Room ",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 3.1386560379517814,
                pitch: 0.17391086148010437,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7881238184917425,
                    pitch: 0.26028599886608994,
                    rotation: 7.0685834705770345,
                    target: "3-fourth-room",
                },
                {
                    yaw: -0.8542047321818629,
                    pitch: 0.2539592441648111,
                    rotation: 5.497787143782138,
                    target: "4-fifth-room-",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.473617108893232,
                    pitch: -0.2648921453145583,
                    title: "Babu",
                    text: "Meet Babu! a sweet dog who came in a year ago. We found Babu in the streets, he was a playful dog. Equally loved by children and adults. Being a trained dog by his previous owners, he was obeying us, helping and consoling little children when they were lost or crying. Babu now enjoys his relaxed home and joyfully jumps when it is time to play.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL5.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "5-sixth-room",
            name: "Sixth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.0924948935169425,
                pitch: 0.0978315766994946,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.758443198210859,
                    pitch: 0.3073331087488693,
                    rotation: 7.0685834705770345,
                    target: "4-fifth-room-",
                },
                {
                    yaw: -0.7987121571269018,
                    pitch: 0.2633466450941615,
                    rotation: 5.497787143782138,
                    target: "6-seventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4487757346454853,
                    pitch: -0.2643435385982684,
                    title: "Yogi",
                    text: "Yogi! As the name says, Yogi is a very relaxed dog. you can find him relaxing and laying down when it is noon. A spirit dog for many of us who loves naps, but do not sleep on his NFT, it is one of the best pieces of art we have ever seen.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL6.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "6-seventh-room",
            name: "Seventh Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.2674595000652484,
                pitch: 0.01653545617547003,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.3375831094752346,
                    pitch: 0.322176705635691,
                    rotation: 11.780972450961727,
                    target: "5-sixth-room",
                },
                {
                    yaw: 0.28971486533178137,
                    pitch: 0.3060841378684067,
                    rotation: 0.7853981633974483,
                    target: "7-eighth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.1217106826903667,
                    pitch: -0.2437502697198326,
                    title: "Bubba",
                    text: "We often listen to the cruel stories of breeders abandoning the dogs once they’re too old to produce more puppies. Such is a story of Bubba, being an older dog, we saw her in a terrible state, treated poorly. She feared us initially, but gained confidence as days passed by and loves her life more now. Her NFTs show her story about what she had been through, being a labrador her talent is limitless.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL7.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.332,
                    pitch: 0.013,
                    id: "#box",
                },

                {
                    yaw: 2.8158,
                    pitch: 0.017,
                    id: "#box",
                },
                {
                    yaw: 3.5124115563955277,
                    pitch: 0.01635741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.994,
                    pitch: 0.01378695413934899,
                    id: "#box",
                },
                {
                    yaw: 5.1176029481141825,
                    pitch: 0.01011,
                    id: "#box",
                },
                {
                    yaw: 4.4537,
                    pitch: 0.01419,
                    id: "#box",
                },
                {
                    yaw: 1.869827474434887,
                    pitch: 0.01,
                    id: "#box",
                },
                {
                    yaw: 1.213,
                    pitch: 0.01,
                    id: "#box",
                },
            ],
        },
        {
            id: "7-eighth-room",
            name: "Eighth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.093401146556234,
                pitch: -0.015841649170663885,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.6956857535477106,
                    pitch: 0.2844639272800116,
                    rotation: 0.7853981633974483,
                    target: "6-seventh-room",
                },
                {
                    yaw: -0.8533222221407613,
                    pitch: 0.2898197106202378,
                    rotation: 5.497787143782138,
                    target: "8-ninth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.469383021047088,
                    pitch: -0.2645498923854124,
                    title: "Tiny ",
                    text: "Meet Tiny, a dog with a big heart and loving nature. She is the quiet one and is often shy. She was welcomed to our shelter recently. With an immediate effect, she made herself at home. After meeting her friends, she became a new dog. She picked up making NFTs and enjoys playing outdoors with her friends. Tiny depicts her kindness in her NFT, her NFT is soothing and Calm. If you are an art lover, this is a must-have look!",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL8.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "8-ninth-room",
            name: "Ninth room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: 3.128774533893174,
                pitch: 0.1426853970313502,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: 0.7727029014521207,
                    pitch: 0.3066958446531629,
                    rotation: 1.5707963267948966,
                    target: "7-eighth-room",
                },
                {
                    yaw: -0.7738449240270313,
                    pitch: 0.27570712570044975,
                    rotation: 5.497787143782138,
                    target: "9-tenth-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.4524140549403155,
                    pitch: -0.26204006318806883,
                    title: "Jury",
                    text: "Meet Jury! When we were driving through the city, we found Jury limping and asking for food in a parking lot. We got down and gave her a few treats. She instantly loved them and started following around. We brought her to the shelter, she later had surgery fixing her rear legs, and now she is a happy, healthy dog. She likes to sit in her cage and make her NFTs.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL9.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.209827474434887,
                    pitch: -0.00019215547101717,
                    id: "#box",
                },
                {
                    yaw: 2.904115563955277,
                    pitch: 0.00835741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.512196818991144,
                    pitch: 0.0078695413934899,
                    id: "#box",
                },
                {
                    yaw: 4.1676029481141825,
                    pitch: 0.004419163220323696,
                    id: "#box",
                },
                {
                    yaw: 4.7237707291474635,
                    pitch: -0.00141928016877586,
                    id: "#box",
                },
                {
                    yaw: 1.625827474434887,
                    pitch: 0.00229215547101717,
                    id: "#box",
                },
            ],
        },
        {
            id: "9-tenth-room",
            name: "Tenth Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -2.359025915836643,
                pitch: 0.06828449628589084,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.26149949637053815,
                    pitch: 0.31571610522741445,
                    rotation: 11.780972450961727,
                    target: "8-ninth-room",
                },
                {
                    yaw: 0.33779885222604733,
                    pitch: 0.30886396976420194,
                    rotation: 0.7853981633974483,
                    target: "10-eleventh-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: -3.1227519968459596,
                    pitch: -0.24376535707700953,
                    title: "Bear",
                    text: "Hello, I’m Bear. I am one of the older dogs here. I love my friend cooper, we both love watching over the younger dogs. Many younger dogs fear injections, so I step up first and show them it is fine to get them. I am one of the first dogs to make my NFT; my NFT represents what I am and my imagination.",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL10.jpg",
                },
            ],
            picture: [
                {
                    yaw: 2.332,
                    pitch: 0.013,
                    id: "#box",
                },

                {
                    yaw: 2.8158,
                    pitch: 0.017,
                    id: "#box",
                },
                {
                    yaw: 3.5124115563955277,
                    pitch: 0.01635741110222514,
                    id: "#box",
                },
                {
                    yaw: 3.994,
                    pitch: 0.01378695413934899,
                    id: "#box",
                },
                {
                    yaw: 5.1176029481141825,
                    pitch: 0.01011,
                    id: "#box",
                },
                {
                    yaw: 4.4537,
                    pitch: 0.01419,
                    id: "#box",
                },
                {
                    yaw: 1.869827474434887,
                    pitch: 0.01,
                    id: "#box",
                },
                {
                    yaw: 1.213,
                    pitch: 0.01,
                    id: "#box",
                },
            ],
        },
        {
            id: "10-eleventh-room",
            name: "Eleventh Room",
            levels: [
                {
                    tileSize: 256,
                    size: 256,
                    fallbackOnly: true,
                },
                {
                    tileSize: 512,
                    size: 512,
                },
                {
                    tileSize: 512,
                    size: 1024,
                },
                {
                    tileSize: 512,
                    size: 2048,
                },
            ],
            faceSize: 2048,
            initialViewParameters: {
                yaw: -3.084522613155819,
                pitch: 0.18014483104191648,
                fov: 1.4108951493785415,
            },
            linkHotspots: [
                {
                    yaw: -0.9040184872902977,
                    pitch: 0.27993471279940607,
                    rotation: 5.497787143782138,
                    target: "9-tenth-room",
                },
                {
                    yaw: 0.8908499690483644,
                    pitch: 0.28329763304182976,
                    rotation: 0.7853981633974483,
                    target: "0-first-room",
                },
            ],
            infoHotspots: [
                {
                    yaw: 2.3257603471356143,
                    pitch: -0.2396146114727653,
                    title: "Vida",
                    text: "Meet Vida! A strong dog. Vida’s energy energizes her friends; she didn’t wait a day to play with her friends after her surgery. Her NFT shows how strong her willpower is. The other dogs aren’t afraid of getting treated after watching Vida happy and healthy again. ",
                    img: "https://www.j69nft.com/Planets/Protected_Paws/img/dogimages/KAL11.jpg",
                },
            ],
            picture: [],
        },
    ],
    name: "Project Title",
    settings: {
        mouseViewMode: "drag",
        autorotateEnabled: false,
        fullscreenButton: false,
        viewControlButtons: false,
    },
};
